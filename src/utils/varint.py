from .conversions import bytes_in_little_endian_to_int, int_to_bytes_in_little_endian

BETWEEN_253_AND_2_POW_16 = 0xFD
BETWEEN_2_POW_16_AND_2_POW_32 = 0xFE
BETWEEN_2_POW_32_AND_2_POW_64 = 0xFF

VARINT_BYTES_NUMBER = {BETWEEN_253_AND_2_POW_16: 2, BETWEEN_2_POW_16_AND_2_POW_32: 4, BETWEEN_2_POW_32_AND_2_POW_64: 8}


def read_varint(stream) -> int:
    """reads an integer from a varint little endian byte stream"""
    first_int = stream.read(1)[0]
    bytes_length = VARINT_BYTES_NUMBER.get(first_int, False)

    if bytes_length:
        return bytes_in_little_endian_to_int(stream.read(bytes_length))
    return first_int


def encode_varint(integer) -> bytes:
    """encodes an integer as a varint bytes"""
    if integer < BETWEEN_253_AND_2_POW_16:
        return bytes([integer])
    elif integer < pow(2, 16):
        return b"\xfd" + int_to_bytes_in_little_endian(integer, 2)
    elif integer < pow(2, 32):
        return b"\xfe" + int_to_bytes_in_little_endian(integer, 4)
    elif integer < pow(2, 64):
        return b"\xff" + int_to_bytes_in_little_endian(integer, 8)
    else:
        raise ValueError(f"Integer too large {integer}")
