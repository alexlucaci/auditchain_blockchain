import hashlib


def hash256(s):
    return hashlib.sha256(hashlib.sha256(s).digest()).digest()


def hash160(s):
    return ripemd160(hashlib.sha256(s).digest())


def ripemd160(s):
    h = hashlib.new("ripemd160")
    h.update(s)
    return h.digest()
