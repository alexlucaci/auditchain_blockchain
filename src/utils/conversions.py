def bytes_in_little_endian_to_int(data: bytes) -> int:
    return int.from_bytes(data, byteorder="little")


def int_to_bytes_in_little_endian(integer: int, number_of_bytes_to_represent: int) -> bytes:
    return integer.to_bytes(number_of_bytes_to_represent, byteorder="little")
