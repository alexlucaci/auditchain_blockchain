import typing

from io import BytesIO
from dataclasses import dataclass

import utils
from .transaction import Transaction


@dataclass
class Block:
    version: int
    previous_block: str
    transactions: typing.List[Transaction]
    timestamp: int

    @classmethod
    def parse(cls, stream: BytesIO) -> "Block":
        version = utils.bytes_in_little_endian_to_int(stream.read(4))
        previous_block = stream.read(32)[::-1].hex()
        number_of_transactions = utils.read_varint(stream)

        transactions = []

        for _ in range(number_of_transactions):
            transactions.append(Transaction.parse(stream))

        timestamp = utils.bytes_in_little_endian_to_int(stream.read(4))

        return cls(version, previous_block, transactions, timestamp)

    def serialize(self) -> bytes:
        version_serialized = utils.int_to_bytes_in_little_endian(self.version, number_of_bytes_to_represent=4)
        previous_block_serialized = bytes.fromhex(self.previous_block)[::-1]
        number_of_transaction_serialized = utils.encode_varint(len(self.transactions))
        timestamp_serialized = utils.int_to_bytes_in_little_endian(self.timestamp, number_of_bytes_to_represent=4)

        serialized_block = bytes()

        serialized_block += version_serialized + previous_block_serialized + number_of_transaction_serialized

        for transaction in self.transactions:
            serialized_block += transaction.serialize()

        serialized_block += timestamp_serialized

        return serialized_block

    def hash(self):
        """Hash in little endian"""
        return utils.hash256(self.serialize())[::-1]

    @property
    def id(self):
        return self.hash().hex()

    def __eq__(self, other: "Block"):
        return self.id == other.id
