import ecdsa
from io import BytesIO
import typing

import utils


class Transaction:
    def __init__(
        self,
        version: int,
        user_id: int,
        event_type: int,
        file_ids: typing.List[int],
        timestamp: int,
        digital_sig: bytes = None,
    ):
        self.version = version
        self.user_id = user_id
        self.event_type = event_type
        self.file_ids = file_ids
        self.timestamp = timestamp
        self.digital_sig = digital_sig
        if self.digital_sig:
            self.is_signed = True
        else:
            self.is_signed = False

    @classmethod
    def parse(cls, stream: BytesIO) -> "Transaction":
        version = utils.bytes_in_little_endian_to_int(stream.read(1))
        user_id = utils.bytes_in_little_endian_to_int(stream.read(4))
        event_type = utils.bytes_in_little_endian_to_int(stream.read(1))
        number_of_files = utils.read_varint(stream)

        file_ids = []

        for _ in range(number_of_files):
            file_ids.append(utils.bytes_in_little_endian_to_int(stream.read(4)))

        timestamp = utils.bytes_in_little_endian_to_int(stream.read(4))
        digital_sig = stream.read(64)

        return cls(version, user_id, event_type, file_ids, timestamp, digital_sig)

    def sign(self, signing_key: ecdsa.SigningKey):
        if self.is_signed:
            raise RuntimeError("Transaction already signed")

        self.is_signed = True
        self.digital_sig = signing_key.sign(self.serialize())

    def verify(self, verifying_key: ecdsa.VerifyingKey) -> bool:
        if not self.is_signed:
            raise RuntimeError("Transaction must be signed before verifying. Use sign() method first")

        try:
            return verifying_key.verify(self.digital_sig, self.data)
        except ecdsa.keys.BadSignatureError:
            return False

    def serialize(self) -> bytes:
        """Returns the byte serialization of the transaction"""

        if not self.is_signed:
            raise RuntimeError("Transaction must be signed before serialization. Use sign() method first")

        version_serialized = utils.int_to_bytes_in_little_endian(self.version, number_of_bytes_to_represent=1)
        user_id_serialized = utils.int_to_bytes_in_little_endian(self.user_id, number_of_bytes_to_represent=4)
        event_type_serialized = utils.int_to_bytes_in_little_endian(self.event_type, number_of_bytes_to_represent=1)
        number_of_files_serialized = utils.encode_varint(len(self.file_ids))

        file_ids_serialized = []

        for file_id in self.file_ids:
            file_ids_serialized.append(utils.int_to_bytes_in_little_endian(file_id, number_of_bytes_to_represent=4))

        timestamp_serialized = utils.int_to_bytes_in_little_endian(self.timestamp, number_of_bytes_to_represent=4)
        if self.digital_sig is None:
            digital_sig_serialized = b""
        else:
            digital_sig_serialized = self.digital_sig

        serialized_transaction = bytes()

        serialized_transaction += (
            version_serialized + user_id_serialized + event_type_serialized + number_of_files_serialized
        )

        for file_id_serialized in file_ids_serialized:
            serialized_transaction += file_id_serialized

        serialized_transaction += timestamp_serialized + digital_sig_serialized

        return serialized_transaction

    def hash(self):
        """Hash in little endian"""
        return utils.hash256(self.serialize())[::-1]

    @property
    def id(self):
        return self.hash().hex()

    @property
    def data(self):
        return self.serialize()[:-64]

    def __eq__(self, other: "Transaction"):
        return self.id == other.id
