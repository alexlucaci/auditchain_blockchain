import asyncio
import logging
import typing

from io import BytesIO

import network
from network import messages

logging.basicConfig(level=logging.NOTSET)
LOGGER = logging.getLogger()


class ConnectionEndedFromClient(Exception):
    pass


class Connection:
    def __init__(self, reader, writer):
        self.reader = reader
        self.writer = writer
        self.handshake_ok = False

    async def send(self, message: messages.Message):
        envelope = network.Envelope(message.command, message.serialize())
        LOGGER.info(f"Sending envelope: {envelope}")
        self.writer.write(envelope.serialize())
        await self.writer.drain()

    async def read(self) -> network.Envelope:
        data = BytesIO(await self.reader.read(1024))
        if data == b"":
            raise ConnectionEndedFromClient
        envelope = network.Envelope.parse(data)
        LOGGER.info(f"Received envelope: {envelope}")

        return envelope

    async def wait_for(self, *message_types: typing.Type[messages.Message]) -> messages.Message:
        """We will only accept a command that it's in the list"""
        command = None
        commands_to_message_type: typing.Dict[bytes, messages.Message] = {
            message_type.command: message_type for message_type in message_types
        }

        envelope = None

        while command not in commands_to_message_type.keys():
            envelope = await self.read()
            command = envelope.command

        LOGGER.info(f"Found message of the expected type: {command.decode('utf-8')}")
        message_type = commands_to_message_type[command]
        return message_type.parse(envelope.stream())

    async def initiate_handshake(self):
        await self.wait_for(messages.Version)

        verack_message = messages.VerAck()
        await self.send(verack_message)
        version_message = messages.Version()
        await self.send(version_message)

        await self.wait_for(messages.VerAck)

        self.handshake_ok = True

        LOGGER.info("Handshake completed")

    async def communicate_with_client(self):
        try:
            await self.initiate_handshake()
        except ConnectionEndedFromClient:
            return

        if self.handshake_ok:
            while True:
                try:
                    message = await self.wait_for(messages.GetBlocks, messages.Blocks, messages.Transactions)
                except ConnectionEndedFromClient:
                    return
                await self.process_response(message)

    async def process_response(self, message: "messages.Message"):
        message_to_return = message.process_request()
        if message_to_return is None:
            return

        await self.send(message_to_return)


class Server:
    def __init__(self, listen_host="0.0.0.0", port=3338):
        self.listen_host = listen_host
        self.port = port
        self.connections = {}

    async def new_connection(self, reader, writer):
        peername = writer.get_extra_info("peername")
        LOGGER.info(f"Connection from {peername}")
        connection = Connection(reader, writer)
        self.connections[peername] = connection
        try:
            await connection.communicate_with_client()
        except network.ConnectionReseted:
            LOGGER.info("Connection ended!")
        del self.connections[peername]
        writer.close()

    async def main(self):
        self.server = await asyncio.start_server(self.new_connection, self.listen_host, self.port)
        print(f"Starting the auditchain node server on port: {self.port}")
        await self.server.serve_forever()

    async def close(self):
        self.server.close()
        await self.server.wait_closed()

    def listen(self):
        try:
            asyncio.run(self.main())
        except asyncio.CancelledError:
            LOGGER.info("Server node gracefully shutdown")
