import socket
import typing
import logging

import network

from network import messages

logging.basicConfig(level=logging.NOTSET)
LOGGER = logging.getLogger()


class Client:
    def __init__(self, host_to_connect_to, port=3338):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host_to_connect_to, port))
        self.stream = self.socket.makefile("rb")
        self.handshake_ok = False

    def close(self):
        self.socket.close()

    def send(self, message: messages.Message):
        envelope = network.Envelope(message.command, message.serialize())
        LOGGER.info(f"Sending envelope: {envelope}")
        self.socket.sendall(envelope.serialize())

    def read(self) -> network.Envelope:
        envelope = network.Envelope.parse(self.stream)
        LOGGER.info(f"Received envelope: {envelope}")

        return envelope

    def wait_for(self, *message_types: typing.Type[messages.Message]) -> messages.Message:
        """We will only accept a command that it's in the list"""
        command = None
        commands_to_message_type: typing.Dict[bytes, messages.Message] = {
            message_type.command: message_type for message_type in message_types
        }

        envelope = None

        while command not in commands_to_message_type.keys():
            envelope = self.read()
            command = envelope.command

        LOGGER.info(f"Found message of the expected type: {command.decode('utf-8')}")
        message_type = commands_to_message_type[command]
        return message_type.parse(envelope.stream())

    def initiate_handshake(self):
        version_message = messages.Version()
        self.send(version_message)
        self.wait_for(messages.VerAck)
        self.wait_for(messages.Version)

        verack_message = messages.VerAck()
        self.send(verack_message)

        self.handshake_ok = True
        LOGGER.info("Handshake completed")

    def get_all_blocks(self):
        self.initiate_handshake()
        get_blocks_message = messages.GetBlocks(
            start_block="d5bcedf3ceec94a7303ec89aad39e203a3447ac33c2ef171f1cbe0961c110bce"
        )

        self.send(get_blocks_message)
        blocks = self.wait_for(messages.Blocks)

        return blocks
