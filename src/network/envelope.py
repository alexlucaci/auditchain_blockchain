from io import BytesIO
from dataclasses import dataclass

import utils


NETWORK_MAGIC = b"\x01\x6f\x4f\x38"


class ConnectionReseted(Exception):
    pass


@dataclass
class Envelope:
    command: bytes
    payload: bytes
    magic: bytes = NETWORK_MAGIC

    def __repr__(self):
        return f"Command: {self.command.decode('ascii')} \n Payload: {self.payload.hex()}"

    @classmethod
    def parse(cls, stream: BytesIO) -> "Envelope":
        magic = stream.read(4)

        if magic == b"":
            raise ConnectionReseted

        if magic != NETWORK_MAGIC:
            raise RuntimeError(f"Network magic is not correct, expected: {NETWORK_MAGIC.hex()} actual: {magic.hex()}")

        command = stream.read(12).strip(b"\x00")
        payload_length = utils.bytes_in_little_endian_to_int(stream.read(4))
        payload_checksum = stream.read(4)
        payload = stream.read(payload_length)

        calculated_checksum = utils.hash256(payload)[:4]

        if calculated_checksum != payload_checksum:
            raise RuntimeError("Checksum does not match!")

        return cls(command, payload)

    def serialize(self) -> bytes:
        magic_serialized = self.magic
        command_serialized = self.command + b"\x00" * (12 - len(self.command))
        payload_length_serialized = utils.int_to_bytes_in_little_endian(
            len(self.payload), number_of_bytes_to_represent=4
        )
        payload_checksum_serialized = utils.hash256(self.payload)[:4]
        payload_serialized = self.payload

        envelope_serialized = magic_serialized + command_serialized + payload_length_serialized

        envelope_serialized += payload_checksum_serialized + payload_serialized

        return envelope_serialized

    def stream(self):
        return BytesIO(self.payload)
