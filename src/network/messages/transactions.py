from io import BytesIO

import utils
import primitives
from .base_message import Message


class Transactions(Message):
    command = b"transaction"

    def __init__(self, transactions):
        self.transactions = transactions

    @classmethod
    def parse(cls, stream: BytesIO) -> "Transactions":
        number_of_transactions = utils.read_varint(stream)

        transactions = []

        for _ in range(number_of_transactions):
            transactions.append(primitives.Transaction.parse(stream))

        return cls(transactions)

    def serialize(self) -> bytes:
        transactions_message_serialized = b""
        number_of_transactions_serialized = utils.encode_varint(len(self.transactions))

        transactions_message_serialized += number_of_transactions_serialized

        for transaction in self.transactions:
            transactions_message_serialized += transaction.serialize()

        return transactions_message_serialized

    def process_request(self):
        pass
