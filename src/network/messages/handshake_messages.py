import time
import random

from .base_message import Message
import utils


class Version(Message):
    command = b"version"

    def __init__(
        self,
        protocol_version=70015,
        network_services=0,
        timestamp=None,
        receiver_services=0,
        receiver_ip=b"\x00\x00\x00\x00",
        receiver_port=8333,
        sender_services=0,
        sender_ip=b"\x00\x00\x00\x00",
        sender_port=8333,
        nonce=None,
        user_agent=b"/auditchain_blockchain:0.1/",
        latest_block=0,
        is_relay=False,
    ):
        self.protocol_version = protocol_version
        self.network_services = network_services

        if timestamp is None:
            self.timestamp = int(time.time())
        else:
            self.timestamp = timestamp

        self.receiver_services = receiver_services
        self.receiver_ip = receiver_ip
        self.receiver_port = receiver_port

        self.sender_services = sender_services
        self.sender_ip = sender_ip
        self.sender_port = sender_port

        if nonce is None:
            self.nonce = utils.int_to_bytes_in_little_endian(random.randint(0, 2 ** 64), 8)
        else:
            self.nonce = nonce

        self.user_agent = user_agent
        self.latest_block = latest_block
        self.is_relay = is_relay

    def serialize(self) -> bytes:
        protocol_version_serialized = utils.int_to_bytes_in_little_endian(self.protocol_version, 4)
        network_services_serialized = utils.int_to_bytes_in_little_endian(self.network_services, 8)
        timestamp_serialized = utils.int_to_bytes_in_little_endian(self.timestamp, 8)

        receiver_services_serialized = utils.int_to_bytes_in_little_endian(self.receiver_services, 8)
        receiver_ip_serialized = b"\x00" * 10 + b"\xff\xff" + self.receiver_ip
        receiver_port_serialized = self.receiver_port.to_bytes(2, "big")

        sender_services_serialized = utils.int_to_bytes_in_little_endian(self.sender_services, 8)
        sender_ip_serialized = b"\x00" * 10 + b"\xff\xff" + self.sender_ip
        sender_port_serialized = self.sender_port.to_bytes(2, "big")

        nonce_serialized = self.nonce
        user_agent_length_serialized = utils.encode_varint(len(self.user_agent))
        user_agent_serialized = self.user_agent

        latest_block_serialized = utils.int_to_bytes_in_little_endian(self.latest_block, 4)

        if self.is_relay:
            is_relay_serialized = b"\x01"
        else:
            is_relay_serialized = b"\x00"

        version_message_serialized = protocol_version_serialized + network_services_serialized + timestamp_serialized

        version_message_serialized += receiver_services_serialized + receiver_ip_serialized + receiver_port_serialized

        version_message_serialized += sender_services_serialized + sender_ip_serialized + sender_port_serialized

        version_message_serialized += nonce_serialized + user_agent_length_serialized + user_agent_serialized

        version_message_serialized += latest_block_serialized + is_relay_serialized

        return version_message_serialized


class VerAck(Message):
    command = b"verack"


class Ping(Message):
    command = b"ping"


class Pong(Message):
    command = b"pong"
