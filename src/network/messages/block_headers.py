from io import BytesIO

import utils
import primitives
from .base_message import Message


class GetHeaders(Message):
    command = b"getheaders"

    def __init__(self, version=70015, num_hashes=1, start_block=None, end_block=None):
        self.version = version
        self.num_hashes = num_hashes
        if start_block is None:
            raise RuntimeError("Start block is required when asking for headers")
        self.start_block = start_block
        if end_block is None:
            self.end_block = b"\x00" * 32
        else:
            self.end_block = end_block

    def serialize(self) -> bytes:
        version_serialized = utils.int_to_bytes_in_little_endian(self.version, 4)
        number_of_hashes_serialized = utils.encode_varint(self.num_hashes)
        start_block_serialized = self.start_block[::-1]
        end_block_serialized = self.end_block[::-1]

        get_headers_message_serialized = (
            version_serialized + number_of_hashes_serialized + start_block_serialized + end_block_serialized
        )

        return get_headers_message_serialized


class Headers(Message):
    command = b"headers"

    def __init__(self, blocks):
        self.blocks = blocks

    @classmethod
    def parse(cls, stream: BytesIO) -> "Headers":
        number_of_headers = utils.read_varint(stream)
        blocks = []

        for _ in range(number_of_headers):
            blocks.append(primitives.Block.parse(stream))
            number_of_transactions = utils.read_varint(stream)

            if number_of_transactions != 0:
                raise RuntimeError("Number of transactions should be 0 in headers message")
        return cls(blocks)
