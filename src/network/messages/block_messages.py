from io import BytesIO
import typing

import plyvel

import utils
import primitives
from .base_message import Message


class GetBlocks(Message):
    command = b"getblocks"

    def __init__(self, version=70015, start_block=None, end_block=None):
        self.version = version
        if start_block is None:
            raise RuntimeError("Hash of start block is required when asking for blocks")
        self.start_block = start_block
        if end_block is None:
            self.end_block = "00" * 32
        else:
            self.end_block = end_block

    @classmethod
    def parse(cls, stream: BytesIO) -> "GetBlocks":
        version = utils.bytes_in_little_endian_to_int(stream.read(4))
        start_block = stream.read(32)[::-1].hex()
        end_block = stream.read(32)[::-1].hex()

        return cls(version, start_block, end_block)

    def serialize(self) -> bytes:
        version_serialized = utils.int_to_bytes_in_little_endian(self.version, 4)
        start_block_serialized = bytes.fromhex(self.start_block)[::-1]
        end_block_serialized = bytes.fromhex(self.end_block)[::-1]

        get_blocks_message_serialized = version_serialized + start_block_serialized + end_block_serialized

        return get_blocks_message_serialized

    def process_request(self) -> "Blocks":
        blocks: typing.List[primitives.Block] = self.get_blocks()

        return Blocks(blocks)

    def get_blocks(self) -> typing.List[primitives.Block]:
        DB = plyvel.DB("storage/auditchain_db/")
        blocks = []
        for block_key, block_path in DB.iterator(prefix=b"block_"):
            with open(block_path, "rb") as f:
                block_data = f.read()
            block = primitives.Block.parse(BytesIO(block_data))
            blocks.append(block)

        return blocks


class Blocks(Message):
    command = b"blocks"

    def __init__(self, blocks):
        self.blocks = blocks

    @classmethod
    def parse(cls, stream: BytesIO) -> "Blocks":
        number_of_blocks = utils.read_varint(stream)
        blocks = []

        for _ in range(number_of_blocks):
            blocks.append(primitives.Block.parse(stream))

        return cls(blocks)

    def serialize(self) -> bytes:
        blocks_serialized = b""
        number_of_blocks_serialized = utils.encode_varint(len(self.blocks))
        blocks_serialized += number_of_blocks_serialized

        for block in self.blocks:
            blocks_serialized += block.serialize()

        return blocks_serialized

    def process_request(self):
        self.store_blocks()

    def store_blocks(self):
        pass
