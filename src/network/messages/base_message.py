from abc import ABC, abstractmethod
from io import BytesIO


class Message(ABC):
    @property
    @abstractmethod
    def command(self) -> bytes:
        return NotImplementedError

    @classmethod
    def parse(cls, stream: BytesIO) -> "Message":
        return cls()

    def serialize(self) -> bytes:
        return b""

    def process_request(self):
        return NotImplementedError
