## Code guidelines
- Use Python3.7
- Line size is 120, the pre-commit hook 'black' will take care of those for you.
- As tox is too slow to be on each commit we are using commit hooks instead to handle style-checks, to set them up run:
    ```
    pip install -r requirements-dev.txt -r test-requirements.txt -r requirements.txt
    pre-commit install
    ```    
- Additionally you can run ```pre-commit autoupdate``` to update all hooks to new versions.
- You can use the ```pre-commit run --all-files``` to run your hooks on all files, useful when adding new hooks.

## CI/CD

This project uses an automated CI pipeline, using [Gitlab CI](https://docs.gitlab.com/ee/ci/quick_start/). Each time a new push is made, a new pipeline will be triggered and automated tests will run over the new changes.

CI configuration is available inside the `.gitlab-ci.yml` file.
