import os
import logging

from io import BytesIO

import plyvel

from network import nodes

logging.basicConfig(level=logging.NOTSET)
LOGGER = logging.getLogger()


if __name__ == "__main__":
    if not os.path.isdir("storage"):
        LOGGER.info("New node detected, will create folder structure for storage")
        os.makedirs("storage/blocks")
        os.makedirs("storage/transactions")
        db = plyvel.DB("storage/auditchain_db/", create_if_missing=True)
        LOGGER.info("Starting to sync and retrieve blocks from other nodes")
        node_client = nodes.Client("localhost", 3338)
        block_message = node_client.get_all_blocks()
        blocks = block_message.blocks
        LOGGER.info(f"Received {len(blocks)} blocks, start storing them")

        for block in blocks:
            with open(f"storage/blocks/{block.id}", "wb") as block_file:
                LOGGER.info(f"Storing block with id {block.id}")
                block_data = BytesIO(block.serialize())
                block_file.write(block_data.getvalue())
                block_path = bytes(os.path.join(os.getcwd(), "storage", "blocks", block.id).encode("utf-8"))
                db.put(b"block_" + bytes(block.id.encode("utf-8")), block_path)
