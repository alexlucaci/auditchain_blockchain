import ecdsa
import time

import primitives


class BaseTest:
    @classmethod
    def setup_class(cls):
        cls.priv_key = ecdsa.SigningKey.from_string(
            string=b"\xcc\xf2\xf4\xb0\xd3\xf44\xf4\xf9\x80jS\x95Mc)\xbf\xf3j\x06\x06\xc2K\r\xd4\x0b\x81Z[\xbc\xf8v",
            curve=ecdsa.SECP256k1,
        )
        cls.pub_key = cls.priv_key.verifying_key

        cls.genesis_transaction = primitives.Transaction(
            version=1, user_id=0, event_type=0, file_ids=[0, 0], timestamp=1580723670
        )

        cls.genesis_transaction.sign(cls.priv_key)

        cls.test_transaction_1 = primitives.Transaction(
            version=1, user_id=14, event_type=1, file_ids=[3, 4, 23], timestamp=int(time.time())
        )

        cls.test_transaction_1.sign(cls.priv_key)

        cls.test_transaction_2 = primitives.Transaction(
            version=1, user_id=11, event_type=2, file_ids=[15], timestamp=1580723670
        )

        cls.test_transaction_2.sign(cls.priv_key)

        cls.genesis_block = primitives.Block(
            version=1, previous_block="00" * 32, transactions=[cls.genesis_transaction], timestamp=1580723670
        )

        cls.test_block_1 = primitives.Block(
            version=1,
            previous_block=cls.genesis_block.id,
            transactions=[cls.test_transaction_1, cls.test_transaction_2],
            timestamp=1580723670,
        )

        cls.test_transaction_3 = primitives.Transaction(
            version=1, user_id=64, event_type=0, file_ids=[100, 15, 12, 33], timestamp=1580723670
        )

        cls.test_transaction_3.sign(cls.priv_key)

        cls.test_invalid_transaction = primitives.Transaction(
            version=1, user_id=64, event_type=0, file_ids=[100, 15, 12, 33], timestamp=1580723670
        )

        cls.test_invalid_transaction.sign(cls.priv_key)
        cls.test_invalid_transaction.user_id = 32

        cls.test_block_2 = primitives.Block(
            version=1, previous_block=cls.test_block_1.id, transactions=[cls.test_transaction_3], timestamp=1580723670
        )
