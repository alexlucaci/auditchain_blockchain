from io import BytesIO

import primitives
from base_test import BaseTest


class TestBlock(BaseTest):
    def test_parse_and_serialize(self):
        genesis_block_raw = self.genesis_block.serialize()

        stream = BytesIO(genesis_block_raw)

        block = primitives.Block.parse(stream)

        assert block.version == 1
        assert block.previous_block == "00" * 32
        assert len(block.transactions) == 1
        assert block.timestamp == 1580723670

        block_serialized = block.serialize()

        assert block_serialized == genesis_block_raw

        test_block_1_raw = self.test_block_1.serialize()
        stream = BytesIO(test_block_1_raw)

        block = primitives.Block.parse(stream)

        assert block.version == 1
        assert block.previous_block == self.genesis_block.id
        assert len(block.transactions) == 2
        assert block.timestamp == 1580723670

        block_serialized = block.serialize()

        assert block_serialized == test_block_1_raw
