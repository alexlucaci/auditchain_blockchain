from io import BytesIO

import primitives
from base_test import BaseTest


class TestTransaction(BaseTest):
    def test_parse_and_serialize_with_one_file(self):
        transaction_serialized = "01140000000101fe000000"
        digital_sig = self.priv_key.sign(transaction_serialized.encode())

        transaction_serialized += "1e77a759" + digital_sig.hex()
        transaction_stream = BytesIO(bytes.fromhex(transaction_serialized))
        transaction = primitives.Transaction.parse(transaction_stream)

        assert transaction.version == 1
        assert transaction.user_id == 20
        assert transaction.event_type == 1
        assert len(transaction.file_ids) == 1
        assert transaction.file_ids[0] == 254
        assert transaction.digital_sig == digital_sig
        assert transaction.timestamp == 0x59A7771E

        assert transaction.serialize().hex() == transaction_serialized

    def test_parse_and_serialized(self):
        transaction_raw = self.test_transaction_3.serialize()

        transaction = primitives.Transaction.parse(BytesIO(transaction_raw))

        assert transaction.version == 1
        assert transaction.user_id == 64
        assert transaction.event_type == 0
        assert transaction.timestamp == 1580723670
        assert len(transaction.file_ids) == 4

        for file_id in [100, 15, 12, 33]:
            assert file_id in transaction.file_ids

        transaction_serialized = transaction.serialize()

        assert transaction_serialized == transaction_raw

    def test_transaction_valid(self):
        for transaction in [self.test_transaction_1, self.test_transaction_2, self.test_transaction_3]:
            assert transaction.verify(self.pub_key)

    def test_transaction_invalid(self):
        assert self.test_invalid_transaction.verify(self.pub_key) is False
