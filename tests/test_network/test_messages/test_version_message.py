from network import messages


class TestVersionMessage:
    def test_serialize(self):
        version_msg_expected = (
            "7f11010000000000000000000000000000000000000000000000000000000000000000000000ffff00000000208d0000"
            "00000000000000000000000000000000ffff00000000208d00000000000000001b2f6175646974636861696e5f626c6f636"
            "b636861696e3a302e312f0000000000".translate({ord(" "): ""})
        )
        version_message = messages.Version(timestamp=0, nonce=b"\x00" * 8)
        assert version_message.serialize().hex() == version_msg_expected
