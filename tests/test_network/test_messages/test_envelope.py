from io import BytesIO

import network

from base_test import BaseTest


class TestEnvelope(BaseTest):
    def test_parse_and_serialize(self):
        message = bytes.fromhex("016f4f3876657261636b000000000000000000005df6e0e2")
        message_stream = BytesIO(message)

        envelope = network.Envelope.parse(message_stream)
        assert envelope.command == b"verack"
        assert envelope.payload == b""
        assert envelope.serialize() == message

        message = bytes.fromhex(
            "016f4f3876657273696f6e0000000000650000005f1a69d2721101000100000000000000bc8f5e540000000001"
            "0000000000000000000000000000000000ffffc61b6409208d010000000000000000000000000000000000ffff"
            "cb0071c0208d128035cbc97953f80f2f5361746f7368693a302e392e332fcf05050001".translate({ord(" "): ""})
        )
        message_stream = BytesIO(message)

        envelope = network.Envelope.parse(message_stream)
        assert envelope.command == b"version"
        assert envelope.payload == message[24:]
        assert envelope.serialize() == message
