from io import BytesIO

import primitives
from base_test import BaseTest
from network import messages


class TestTransactions(BaseTest):
    @classmethod
    def setup_class(cls):
        super().setup_class()
        cls.transaction_message = messages.Transactions([cls.test_transaction_2, cls.test_transaction_3])

    def test_parse(self):
        transaction_1_serialized = "01140000000101fe000000"
        digital_sig_1 = self.priv_key.sign(transaction_1_serialized.encode())
        transaction_1_serialized += "1e77a759" + digital_sig_1.hex()

        transaction_2_serialized = "01c3000000020154280000"
        digital_sig_2 = self.priv_key.sign(transaction_2_serialized.encode())
        transaction_2_serialized += "1e77a759" + digital_sig_2.hex()

        transaction_msg_expected = "02" + transaction_1_serialized + transaction_2_serialized

        stream = BytesIO(bytes.fromhex(transaction_msg_expected))
        transactions_message = messages.Transactions.parse(stream)
        assert len(transactions_message.transactions) == 2
        for transaction in transactions_message.transactions:
            assert transaction.__class__ == primitives.Transaction

        # Tests below are extra and don't have anything in common with network messages

        transaction_1_received_serialized, transaction_2_received_serialized = transactions_message.transactions

        assert transaction_1_received_serialized.version == 1
        assert transaction_1_received_serialized.user_id == 20
        assert transaction_1_received_serialized.event_type == 1
        assert len(transaction_1_received_serialized.file_ids) == 1
        assert transaction_1_received_serialized.file_ids[0] == 254
        assert transaction_1_received_serialized.digital_sig == digital_sig_1
        assert transaction_1_received_serialized.timestamp == 0x59A7771E

        assert transaction_2_received_serialized.version == 1
        assert transaction_2_received_serialized.user_id == 195
        assert transaction_2_received_serialized.event_type == 2
        assert len(transaction_2_received_serialized.file_ids) == 1
        assert transaction_2_received_serialized.file_ids[0] == 10324
        assert transaction_2_received_serialized.digital_sig == digital_sig_2
        assert transaction_2_received_serialized.timestamp == 0x59A7771E

    def test_parse_and_serialize(self):
        raw_transaction_message = self.transaction_message.serialize()
        transactions_message = messages.Transactions.parse(BytesIO(raw_transaction_message))

        for transaction in [self.test_transaction_2, self.test_transaction_3]:
            assert transaction in transactions_message.transactions

        assert transactions_message.serialize() == raw_transaction_message
