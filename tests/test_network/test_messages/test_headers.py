from io import BytesIO

import primitives
from network import messages

import pytest


class TestHeaders:
    @pytest.mark.skip
    def test_parse(self):
        headers_msg_expected = (
            "02"
            "00000020df3b053dc46f162a9b00c7f0d5124e2676d47bbe7c5d0793a500000000000000dc7c835b"
            "00"
            "0000002030eb2540c41025690160a1014c577061596e32e426b712c7ca000000000000004880835b"
            "00".translate({ord(" "): ""})
        )
        stream = BytesIO(bytes.fromhex(headers_msg_expected))
        headers = messages.Headers.parse(stream)
        assert len(headers.blocks) == 2
        for block in headers.blocks:
            assert block.__class__ == primitives.Block
