from io import BytesIO

from base_test import BaseTest

from network import messages


class TestBlocks(BaseTest):
    @classmethod
    def setup_class(cls):
        super().setup_class()
        cls.get_blocks_message = messages.GetBlocks(start_block=cls.genesis_block.id)
        cls.blocks_message = messages.Blocks(blocks=[cls.test_block_1, cls.test_block_2])

    def test_getblocks_parse_serialize(self):
        raw_getblocks_message = self.get_blocks_message.serialize()
        getblocks_message = messages.GetBlocks.parse(BytesIO(raw_getblocks_message))

        assert getblocks_message.start_block == self.genesis_block.id
        assert getblocks_message.end_block == "00" * 32

        assert getblocks_message.serialize() == raw_getblocks_message

    def test_blocks_parse_serialize(self):
        raw_blocks_messages = self.blocks_message.serialize()
        blocks_message = messages.Blocks.parse(BytesIO(raw_blocks_messages))

        assert len(blocks_message.blocks) == 2

        for block in [self.test_block_1, self.test_block_2]:
            assert block in blocks_message.blocks

        assert blocks_message.serialize() == raw_blocks_messages
