from network import messages


class TestGetHeaders:
    def test_serialize(self):
        get_headers_msg_expected = (
            "7f11010001a35bd0ca2f4a88c4eda6d213e2378a5758dfcd6af43712000000000000000000000"
            "0000000000000000000000000000000000000000000000000000000000000".translate({ord(" "): ""})
        )
        start_block_hex = "0000000000000000001237f46acddf58578a37e213d2a6edc4884a2fcad05ba3"
        get_headers_message = messages.GetHeaders(start_block=bytes.fromhex(start_block_hex))

        assert get_headers_message.serialize().hex() == get_headers_msg_expected
