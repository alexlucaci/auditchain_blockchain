import time
import random
import threading

import pytest

from network import nodes
from network import messages
from base_test import BaseTest


class TestNode(BaseTest):
    @pytest.mark.asyncio
    async def test_handshake(self):
        port = random.randint(50000, 60000)
        server = nodes.Server(port=port)
        server_thread = threading.Thread(target=server.listen)
        server_thread.start()

        # Wait a moment until the server thread binds
        time.sleep(0.1)

        client = nodes.Client("localhost", port=port)
        client.initiate_handshake()
        assert client.handshake_ok is True

        block_message = messages.Blocks([self.genesis_block])
        client.send(block_message)

        await server.close()
        server_thread.join()
